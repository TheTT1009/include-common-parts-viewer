package jp.co.kodnet.confluence.plugins.actions;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.api.service.content.ContentBodyConversionService;
//import com.atlassian.confluence.api.service.content.ContentBodyConversionService;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import java.util.List;
import com.atlassian.confluence.content.render.xhtml.FormatConverter;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.SpaceLabelManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.google.gson.Gson;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import javax.servlet.http.HttpServlet;
import jp.co.kodnet.confluence.plugins.common.Constants;
import jp.co.kodnet.confluence.plugins.includecommon.beans.DataBean;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.cyberneko.html.parsers.DOMParser;

import org.w3c.dom.*;
import org.xml.sax.InputSource;


/*
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
 */
public class CommonPartsAction extends HttpServlet {

    private final ContentPropertyManager contentPropertyManager;
    private final BandanaManager bandanaManager;
    private final PageManager pageManager;
    private final PermissionManager permissionManager;
    private final SpaceManager spaceManager;
    private final ContentBodyConversionService contentBodyConversionService;
    private final FormatConverter formatConverter;
    private I18NBeanFactory i18NBeanFactory;
    private final SpaceLabelManager spaceLabelManager;
    private final LabelManager labelManager;
    private final AttachmentManager attachmentManager;
    private int dataId;

    /**
     *
     * @param pageManager
     * @param contentPropertyManager
     * @param permissionManager
     * @param bandanaManager
     */
    public CommonPartsAction(
            PageManager pageManager, ContentPropertyManager contentPropertyManager,
            PermissionManager permissionManager, BandanaManager bandanaManager,
            SpaceManager spaceManager, ContentBodyConversionService contentBodyConversionService,
            FormatConverter formatConverter, I18NBeanFactory i18NBeanFactory, SpaceLabelManager spaceLabelManager,
            LabelManager labelManager, AttachmentManager attachmentManager
    ) {
        this.contentPropertyManager = contentPropertyManager;
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
        this.bandanaManager = bandanaManager;
        this.spaceManager = spaceManager;
        this.contentBodyConversionService = contentBodyConversionService;
        this.formatConverter = formatConverter;
        this.i18NBeanFactory = i18NBeanFactory;
        this.spaceLabelManager = spaceLabelManager;
        this.labelManager = labelManager;
        this.attachmentManager = attachmentManager;
    }

    // <editor-fold defaultstate="collapsed" desc="protected">
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Gson gson = new Gson();
        boolean result = true;
        int resStatus = HttpServletResponse.SC_OK;
        String tmpMsg = null;
        String htmlText = "";
        String pageId = request.getParameter(Constants.PARAM_SOURCE_PAGE_ID);
        Page targetPage = pageManager.getPage(Integer.parseInt(pageId));
        htmlText = formatConverter.convertToEditorFormat(targetPage.getBodyAsString(), new PageContext(targetPage.getEntity()));

        String srcpageId = request.getParameter(Constants.PARAM_SOURCE_PAGE_ID);
        String dstpageId = request.getParameter(Constants.PARAM_DEST_PAGE_ID);
        int srcPageID = Integer.parseInt(srcpageId);
        int dstPageID = Integer.parseInt(dstpageId);
        Page dstPage = pageManager.getPage(dstPageID);

        try {
            copyAndReplaceAttachemt(htmlText, srcPageID, dstPageID);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CommonPartsAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        htmlText = formatConverter.convertToEditorFormat(targetPage.getBodyAsString(), new PageContext(dstPage.getEntity()));

        //JSONデータ設定
        DataBean res = new DataBean(result, tmpMsg, htmlText);
        response.setStatus(resStatus);
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(gson.toJson(res));
    }

    private void copyAndReplaceAttachemt(String includePageBody, int srcPageID, int dstPageID) throws Exception {
        try {
            DOMParser parser = new DOMParser();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(includePageBody));
            parser.parse(is);
            Document doc = parser.getDocument();
            String macroName, macroPara, imageName, attachmentName;
            NodeList nodes = doc.getElementsByTagName("img");
            NodeList linkNodes = doc.getElementsByTagName("a");
            for (int i = 0; i < linkNodes.getLength(); i++) {
                Node nNode = linkNodes.item(i);
                if (nNode.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }

                Element e = (Element) nNode;
                macroName = e.getAttribute(Constants.PARAM_DATA_RESOURCE_TYPE);
                if (macroName.equals("attachment")) {
                    attachmentName = e.getAttribute(Constants.PARAM_DATA_RESOURCE_ALIAS);
                    copyAttachment(attachmentName, srcPageID, dstPageID);
                }
            }

            for (int i = 0; i < nodes.getLength(); i++) {
                Node nNode = nodes.item(i);
                if (nNode.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) nNode;
                macroName = e.getAttribute(Constants.PARAM_DATA_MACRO_NAME);
                imageName = e.getAttribute(Constants.PARAM_DATA_RESOURCE_ALIAS);

                if (!imageName.isEmpty()) {
                    attachmentName = imageName;
                    copyAttachment(attachmentName, srcPageID, dstPageID);

                } else if (macroName.equals(Constants.PARAM_ATTACHMENT_TYPE_FILE)) {
                    macroPara = e.getAttribute(Constants.PARAM_DATA_MACRO_PARAMETER);
                    if (macroPara.isEmpty()) {
                        continue;
                    } else if (macroPara.contains(Constants.PARAM_MACRO_ATTRIBUTE_NAME)) {
                        attachmentName = this.getNameOfMacro(macroPara, Constants.PARAM_MACRO_ATTRIBUTE_NAME);
                        copyAttachment(attachmentName, srcPageID, dstPageID);
                        Page dstPage = pageManager.getPage(dstPageID);
                        getAttachFileName(dstPage, attachmentName);
                        continue;
                    }
                }
                if (macroName.equals(Constants.PARAM_ATTACHMENT_TYPE_GLIFFY)) {
                    macroPara = e.getAttribute(Constants.PARAM_DATA_MACRO_PARAMETER);
                    if (macroPara.contains(Constants.PARAM_MACRO_ATTRIBUTE_NAME)) {
                        attachmentName = this.getNameOfMacro(macroPara, Constants.PARAM_MACRO_ATTRIBUTE_NAME);
                        copyAttachment(attachmentName, srcPageID, dstPageID);
                        String gliffyName = attachmentName + (".png");
                        if (gliffyName.equals(("") + (".png"))) {
                            continue;
                        }
                        copyAttachment(gliffyName, srcPageID, dstPageID);
                        String gliffyHtmlname = attachmentName + (".html");
                        if (gliffyHtmlname.equals(("") + (".html"))) {
                            continue;
                        }
                        copyAttachment(gliffyHtmlname, srcPageID, dstPageID);
                    }
                }

                if (macroName.equals(Constants.PARAM_ATTACHMENT_TYPE_DRAWIO)) {
                    macroPara = e.getAttribute(Constants.PARAM_DATA_MACRO_PARAMETER);
                    if (macroPara.contains(Constants.PARAM_GLIFFY_NAME)) {
                        attachmentName = this.getNameOfMacro(macroPara, Constants.PARAM_GLIFFY_NAME);
                        copyAttachment(attachmentName, srcPageID, dstPageID);
                        String drawioName = attachmentName + (".png");
                        copyAttachment(drawioName, srcPageID, dstPageID);
                    }
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    private void copyAttachment(String attachmentName, int srcPageID, int dstPageID) throws Exception {
        Page srcPage = pageManager.getPage(srcPageID);
        Page dstPage = pageManager.getPage(dstPageID);
        if (srcPage == null || dstPage == null || attachmentName == null) {
            return;
        }

        Attachment attachment = attachmentManager.getAttachment(srcPage, attachmentName);
        if (attachment == null) {
            return;
        }

        List<Attachment> lstVersionAttachment = attachmentManager.getAllVersions(attachment);

        Collections.sort(lstVersionAttachment, new Comparator<Attachment>() {
            public int compare(Attachment o1, Attachment o2) {
                if (o1 == null) {
                    return 0;
                }
                if (o1.getVersion() < o2.getVersion()) {
                    return o1.getVersion() - o2.getVersion();
                }
                return o2.getVersion() - o1.getVersion();
            }
        });

        Attachment oldAttachment = null;
        for (int i = 0; i < lstVersionAttachment.size(); i++) {
            Attachment currentAttach = lstVersionAttachment.get(i);
            Attachment newAttachment = new Attachment();
            InputStream data = new DataInputStream(attachmentManager.getAttachmentData(currentAttach));
            if (i == 0) {
                newAttachment.setFileName(currentAttach.getFileName());
                newAttachment.setFileSize(currentAttach.getFileSize());
                newAttachment.setMediaType(currentAttach.getMediaType());
                newAttachment.setVersionComment(currentAttach.getVersionComment());
                newAttachment.setVersion(currentAttach.getVersion());
                dstPage.addAttachment(newAttachment);
                attachmentManager.saveAttachment(newAttachment, null, data);
                newAttachment.setFileName(getAttachFileName(dstPage, attachmentName));
            } else {
                newAttachment = attachmentManager.getAttachment(dstPage, attachmentName, i);
                oldAttachment = (Attachment) newAttachment.clone();
                oldAttachment.convertToHistoricalVersion();
                newAttachment.setFileName(currentAttach.getFileName());
                newAttachment.setMediaType(currentAttach.getMediaType());
                newAttachment.setFileSize(currentAttach.getFileSize());
                newAttachment.setVersionComment(currentAttach.getVersionComment());
                attachmentManager.saveAttachment(newAttachment, oldAttachment, data);
                newAttachment.setFileName(getAttachFileName(dstPage, attachmentName));
            }
        }
    }

    private String getAttachFileName(Page dstPage, String attachFileName) {
        int i = 0;
        String fileName = attachFileName;
        String fileNameWithoutExtension = FilenameUtils.removeExtension(attachFileName);
        String extensionName = FilenameUtils.getExtension(attachFileName);
        while (true) {
            Attachment currentAtt = attachmentManager.getAttachment(dstPage, fileName);
            if (currentAtt == null) {
                return fileName;
            }
            
            i++;
            fileName = fileNameWithoutExtension + "_" + i;
            if (!extensionName.isEmpty()) {
                fileName = fileName + "." + extensionName;
            }
        } 
    }

    private String getNameOfMacro(String macroAttrPara, String nameMark) throws Exception {
        String macroAttrName;
        int beginIndex = macroAttrPara.indexOf(nameMark);
        int endIndex = macroAttrPara.indexOf("|", beginIndex + nameMark.length());

        if (beginIndex < -1) {
            return "";
        }
        if (endIndex > -1) {
            macroAttrName = macroAttrPara.substring(beginIndex + nameMark.length(), endIndex);
        } else {
            macroAttrName = macroAttrPara.substring(beginIndex + nameMark.length());
        }
        return macroAttrName;
    }

    private void checkGliffyMacro(String htmlText, int srcPageID, int dstPageID) throws Exception {
        //DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        DOMParser parser = new DOMParser();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(htmlText));
        //Document doc = db.parse(is);
        parser.parse(is);
        Document doc = parser.getDocument();
        NodeList nodes = doc.getElementsByTagName("img");
        String macroName, macroPara, name;
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);
            macroName = element.getAttribute("data-macro-name");

            if (macroName.equals("gliffy")) {
                macroPara = element.getAttribute("data-macro-parameters");
                if (macroPara.startsWith("name=")) {
                    int index = macroPara.indexOf("=");
                    if (index > -1) {
                        name = macroPara.substring(index + 1);

                        String pngName = name + ".png";
                        String gliffyName = name;
                        Page srcPage = pageManager.getPage(srcPageID);
                        Page dstPage = pageManager.getPage(dstPageID);
                        if (srcPage != null && dstPage != null) {
                            List<Attachment> srcPageattachments = srcPage.getLatestVersionsOfAttachments();
                            for (Attachment srcPageattachment : srcPageattachments) {
                                if (srcPageattachment.getFileName().equals(pngName) || srcPageattachment.getFileName().equals(gliffyName)) {
                                    //List<Attachment> dstPageattachments = dstPage.getLatestVersionsOfAttachments();
                                    //dstPageattachments.add(dstPageattachments.size(), srcPageattachment);
                                    Attachment a = new Attachment();
                                    a.setFileName(srcPageattachment.getFileName());
                                    long attrsize = 0;
                                    attrsize = srcPageattachment.getFileSize();
                                    a.setFileSize(attrsize);
                                    a.setMediaType(srcPageattachment.getMediaType());
                                    a.setVersionComment(srcPageattachment.getVersionComment());
                                    a.setVersion(1);
                                    dstPage.addAttachment(a);

                                    InputStream data = null;
                                    try {
                                        //attachmentManager.copyAttachment(tmp, homePage);
                                        data = attachmentManager.getAttachmentData(srcPageattachment);
                                        attachmentManager.saveAttachment(a, null, data);
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    } finally {
                                        if (data != null) {
                                            IOUtils.closeQuietly(data);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // </editor-fold>
}
