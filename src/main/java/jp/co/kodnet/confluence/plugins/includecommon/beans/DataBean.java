/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.kodnet.confluence.plugins.includecommon.beans;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author TAMPT
 */
public class DataBean {

    public boolean result;
    public String msg;
    public String html;

    public DataBean() {
        this.result = true;
        this.msg = StringUtils.EMPTY;
        this.html = StringUtils.EMPTY;
    }

    public DataBean(boolean result, String msg) {
        this.result = result;
        this.msg = msg;
        this.html = StringUtils.EMPTY;
    }

    public DataBean(boolean result, String msg, String html) {
        this.result = result;
        this.msg = msg;
        this.html = html;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Map<String, Object> getMapData() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("msg", this.msg);
        data.put("result", this.result);
        data.put("html", this.html);
        return data;
    }
}
