package jp.co.kodnet.confluence.plugins.actions;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import static com.atlassian.gzipfilter.org.apache.commons.lang.StringEscapeUtils.escapeHtml;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.opensymphony.xwork.ActionContext;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.common.ActionContextHelper;
import jp.co.kodnet.confluence.plugins.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.common.Constants;
import jp.co.kodnet.confluence.plugins.includecommon.beans.DataBean;

/**
 *
 * @author TAMPT
 */
public class SearchAction extends ConfluenceActionSupport implements Beanable {

    // <editor-fold defaultstate="collapsed" desc="variable">  
    private DataBean dataBean = new DataBean();
    private SpaceManager spaceManager;
    private PageManager pageManager;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public">  
    /**
     * Jsonデータの定義
     *
     * @return
     */
    public Object getBean() {
        return this.dataBean.getMapData();
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    public String execute() throws Exception {
        ActionContext context = ActionContext.getContext();
        String spaceTitle = ActionContextHelper.getFirstParameterValueAsString(context, Constants.PARAM_SPACE_TITLE);
        String spaceCategory = ActionContextHelper.getFirstParameterValueAsString(context, Constants.PARAM_SPACE_CATEGORY);
        String pageTitle = ActionContextHelper.getFirstParameterValueAsString(context, Constants.PARAM_PAGE_TITLE);
        String pageLabel = ActionContextHelper.getFirstParameterValueAsString(context, Constants.PARAM_PAGE_LABEL);
        String dstPageId = ActionContextHelper.getFirstParameterValueAsString(context, Constants.PARAM_DEST_PAGE_ID);

        //データの取得
        Map<Page, String> dataResult = this.getDataSearch(spaceTitle, spaceCategory, pageTitle, pageLabel);
        //HTMLの取得
        String html = this.getRenderHtmlSearch(dataResult, dstPageId);

        this.dataBean = new DataBean(true, StringUtils.EMPTY, html);
        return SUCCESS;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="private">
    /**
     * 検索結果のデータの取得
     *
     * @param spaceTitle　スペースタイトル
     * @param spaceCategory　スペースカテゴリ
     * @param pageTitle　ページタイトル
     * @param pageCategory　ページカテゴリ
     * @return
     */
    private Map<Page, String> getDataSearch(String spaceTitle, String spaceCategory, String pageTitle, String pageLabel) {
        Map<Page, String> dataResult = new LinkedHashMap<Page, String>();

        //全てスペースの取得
        List<Space> lstSpace = this.spaceManager.getAllSpaces();
        boolean isSpaceTitle = CommonFunction.isNullOrEmpty(spaceTitle);
        boolean isSpaceCategory = CommonFunction.isNullOrEmpty(spaceCategory);
        boolean isPageTitle = CommonFunction.isNullOrEmpty(pageTitle);
        boolean isPageLabel = CommonFunction.isNullOrEmpty(pageLabel);

        for (Space space : lstSpace) {
            //スペースタイトルのチェック
            if (!isSpaceTitle) {
                if (!space.getDisplayTitle().contains(spaceTitle)) {
                    continue;
                }
            }

            //スペースカテゴリのチェック
            if (!isSpaceCategory) {
                List<Label> lstLabel = space.getDescription().getLabels();
                boolean isExistedLabel = false;
                for (Label label : lstLabel) {
                    if (label.getDisplayTitle().equals(spaceCategory)) {
                        isExistedLabel = true;
                    }
                }
                if (!isExistedLabel) {
                    continue;
                }
            }

            //ページタイトルの存在場合、ページカテゴリの存在場合
            for (Page page : pageManager.getPages(space, true)) {
                //ページタイトルのチェック
                if (!isPageTitle) {
                    if (!page.getDisplayTitle().contains(pageTitle)) {
                        continue;
                    }
                }

                //ページカテゴリのチェック
                boolean isExistedLabel = true;
                if (!isPageLabel) {
                    isExistedLabel = false;
                    for (Label label : page.getLabels()) {
                        if (label.getDisplayTitle().equals(pageLabel)) {
                            isExistedLabel = true;
                            break;
                        }
                    }
                }

                if (!isExistedLabel) {
                    continue;
                }
                dataResult.put(page, space.getDisplayTitle());
                if (dataResult.size() == 100) {
                    break;
                }
            }

            if (dataResult.size() == 100) {
                break;
            }
        }

        return dataResult;
    }

    /**
     * HTML結果の取得
     *
     * @param dataResult 検索結果のデータ
     * @return
     */
    private String getRenderHtmlSearch(Map<Page, String> dataResult, String dstPageId) {

        I18NBean i18NBean = this.i18NBeanFactory.getI18NBean(getLocale());
        StringBuilder stringResult = new StringBuilder();

        if (dataResult.isEmpty()) {
            stringResult.append("<div class=\"message-panel message-handler\" style='margin:10px;'>");
            stringResult.append(i18NBean.getText("kod.plugin.includepage.search.result.empty"));
            stringResult.append("</div>");
            return stringResult.toString();
        }

        int pageId = Integer.parseInt(dstPageId);
        Page currentPage = pageManager.getPage(pageId);
        int count = 0;
        stringResult.append("<select class='multi-select' size='13' style='height:110px; max-width: 100%; margin-top: 3px; overflow:auto;'>");
        for (Map.Entry<Page, String> item : dataResult.entrySet()) {
            Page page = item.getKey();
            String spaceTitle = item.getValue();
            stringResult.append("<option data-pageId='");
            stringResult.append(page.getIdAsString());
            stringResult.append("' value='");
            stringResult.append(page.getIdAsString());
            stringResult.append("' data-page='");
            if (currentPage.getSpaceKey().equals(page.getSpaceKey())) {
                stringResult.append(escapeHtml(page.getTitle()));
            } else {
                stringResult.append(escapeHtml(page.getSpaceKey())).append(":").append(escapeHtml(page.getTitle()));
            }

            if (count == 0) {
                stringResult.append("' selected>");
            } else {
                stringResult.append("'>");
            }
            stringResult.append(escapeHtml(spaceTitle));
            stringResult.append(":");
            stringResult.append(escapeHtml(page.getDisplayTitle()));
            stringResult.append("</option>");
            count++;
        }

        stringResult.append("</select>");
        return stringResult.toString();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="get/set">
    /**
     * @return the spaceManager
     */
    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    /**
     * @param spaceManager the spaceManager to set
     */
    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    /**
     * @return the pageManager
     */
    public PageManager getPageManager() {
        return pageManager;
    }

    /**
     * @param pageManager the pageManager to set
     */
    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }
    // </editor-fold>
}
