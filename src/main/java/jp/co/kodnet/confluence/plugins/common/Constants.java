/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.kodnet.confluence.plugins.common;

/**
 *
 * @author TAMPT
 */
public class Constants {
    // <editor-fold defaultstate="collapsed" desc="common">  
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="page select">  
    /*パラメータ*/
    public static String PARAM_SPACE_TITLE = "spaceTitle";
    public static String PARAM_SPACE_CATEGORY = "spaceCategory";
    public static String PARAM_PAGE_TITLE = "pageTitle";
    public static String PARAM_PAGE_LABEL = "pageLabel";
    public static String PARAM_SOURCE_PAGE_ID = "srcPageId";
    public static String PARAM_DEST_PAGE_ID = "destPageId";
    public static String PARAM_MACRO_ATTRIBUTE_NAME = "name=";
    public static String PARAM_DATA_RESOURCE_TYPE = "data-linked-resource-type";
    public static String PARAM_DATA_RESOURCE_ALIAS = "data-linked-resource-default-alias";
    public static String PARAM_DATA_MACRO_PARAMETER = "data-macro-parameters";
    public static String PARAM_ATTACHMENT_TYPE_DRAWIO = "drawio";
    public static String PARAM_ATTACHMENT_TYPE_GLIFFY = "gliffy";
    public static String PARAM_ATTACHMENT_TYPE_FILE = "view-file";
    public static String PARAM_DATA_MACRO_NAME = "data-macro-name";
    public static String PARAM_GLIFFY_NAME = "diagramName=";
    
    // </editor-fold>
}
