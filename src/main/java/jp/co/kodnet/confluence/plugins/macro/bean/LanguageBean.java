package jp.co.kodnet.confluence.plugins.macro.bean;

/**
 *
 * @author hailc
 */
public class LanguageBean {

    private String langKey;
    private String langName;

    public LanguageBean(String langKey, String langName) {
        this.langKey = langKey;
        this.langName = langName;
    }

    /**
     * @return the langKey
     */
    public String getLangKey() {
        return langKey;
    }

    /**
     * @param langKey the langKey to set
     */
    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    /**
     * @return the langName
     */
    public String getLangName() {
        return langName;
    }

    /**
     * @param langName the langName to set
     */
    public void setLangName(String langName) {
        this.langName = langName;
    }
}
