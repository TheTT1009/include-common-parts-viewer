package jp.co.kodnet.confluence.plugins.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.opensymphony.xwork.ActionContext;

public class ActionContextHelper {

    private ActionContextHelper() {
    }

    public static String getFirstParameterValueAsString(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return ((String[]) paramValues)[0];
        } else if (paramValues instanceof String) {
            return (String) paramValues;
        }
        return null;
    }

    public static List<String> getParameterValuesAsStringList(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return Arrays.asList((String[]) paramValues);
        } else if (paramValues instanceof String) {
            return Collections.singletonList((String) paramValues);
        }
        return Collections.emptyList();
    }

    public static boolean getFirstCheckBoxParameterValueAsBoolean(ActionContext context, String key) {
        String value = getFirstParameterValueAsString(context, key);
        return value != null && value.equalsIgnoreCase("on");
    }

    public static int getFirstParameterValueAsInt(ActionContext context, String key, int defaultValue) {
        String stringValue = getFirstParameterValueAsString(context, key);
        if (stringValue == null || !stringValue.matches("[0-9]+")) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(stringValue);
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return defaultValue;
        }
    }

    public static long getFirstParameterValueAsLong(ActionContext context, String key, long defaultValue) {
        String stringValue = getFirstParameterValueAsString(context, key);
        if (stringValue == null || !stringValue.matches("[0-9]+")) {
            return defaultValue;
        }
        try {
            return Long.parseLong(stringValue);
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return defaultValue;
        }
    }
}
