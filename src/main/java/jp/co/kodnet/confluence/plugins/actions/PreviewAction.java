/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.kodnet.confluence.plugins.actions;

import com.atlassian.confluence.content.render.xhtml.FormatConverter;
import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.opensymphony.xwork.ActionContext;
import jp.co.kodnet.confluence.plugins.common.ActionContextHelper;
import jp.co.kodnet.confluence.plugins.common.Constants;
import jp.co.kodnet.confluence.plugins.includecommon.beans.DataBean;

/**
 *
 * @author TAMPT
 */
public class PreviewAction extends ConfluenceActionSupport implements Beanable {

    // <editor-fold defaultstate="collapsed" desc="variable">  
    private DataBean dataBean = new DataBean();
    private SpaceManager spaceManager;
    private PageManager pageManager;
    private FormatConverter formatConverter;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public">
    /**
     * Jsonデータの定義
     *
     * @return
     */
    public Object getBean() {
        return this.dataBean.getMapData();
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    public String execute() throws Exception {
        ActionContext context = ActionContext.getContext();
        String pageParam = ActionContextHelper.getFirstParameterValueAsString(context, "page");
        String dstPageId = ActionContextHelper.getFirstParameterValueAsString(context, Constants.PARAM_DEST_PAGE_ID);
        int pageId = Integer.parseInt(dstPageId);
        Page currentPage = pageManager.getPage(pageId);
        String spaceKey, pageTitle = "";
        spaceKey = currentPage.getSpaceKey();
        Page includePage = this.pageManager.getPage(spaceKey, pageParam);

        if (includePage == null) {
            if (pageParam.indexOf(":") > 0) {
                int endSubIndex = pageParam.indexOf(":");
                spaceKey = pageParam.substring(0, endSubIndex);
                pageTitle = pageParam.substring(endSubIndex + 1);
            }
            includePage = this.pageManager.getPage(spaceKey, pageTitle);
        }

        String strContents;
        if (includePage == null) {
            strContents = this.getText("page.not.found.error");
        } else {
            strContents = formatConverter.convertToViewFormat(includePage.getBodyAsString(), new PageContext(includePage.getEntity()));
        }

        StringBuilder strPreviewResult = new StringBuilder();
        strPreviewResult.append("<div id='main'>");
        strPreviewResult.append("<div id='content' class='page edit'>");
        strPreviewResult.append("<div class='wiki-content'>");
        strPreviewResult.append(strContents);
        strPreviewResult.append("</div>");
        strPreviewResult.append("</div>");
        strPreviewResult.append("</div>");

        this.dataBean = new DataBean(true, StringUtils.EMPTY, strPreviewResult.toString());
        return SUCCESS;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="get/set">
    /**
     * @return the spaceManager
     */
    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    /**
     * @param spaceManager the spaceManager to set
     */
    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    /**
     * @return the pageManager
     */
    public PageManager getPageManager() {
        return pageManager;
    }

    /**
     * @param pageManager the pageManager to set
     */
    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    /**
     * @return the formatConverter
     */
    public FormatConverter getFormatConverter() {
        return formatConverter;
    }

    /**
     * @param formatConverter the formatConverter to set
     */
    public void setFormatConverter(FormatConverter formatConverter) {
        this.formatConverter = formatConverter;
    }
    // </editor-fold>
}
