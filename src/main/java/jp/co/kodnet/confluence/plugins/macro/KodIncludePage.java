package jp.co.kodnet.confluence.plugins.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.FormatConverter;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactoryFactoryBean;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xml.HTMLParagraphStripper;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Nhatnq
 */
public class KodIncludePage implements Macro {

    // <editor-fold defaultstate="collapsed" desc="【変数】">
    private PageManager pageManager;
    private I18NBeanFactory i18NBeanFactory;
    private final HTMLParagraphStripper htmlParagraphStripper;
    private Map<String, String> listUpdateMacroDuplicate;
    private String outputType = "";
    private LocaleManager localeManager;
    private final String PAGE_PARAMETER = "page";
    private Renderer viewRenderer;
    private FormatConverter formatConverter;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="【公開メソッド】">
    @Autowired
    public KodIncludePage() {
        listUpdateMacroDuplicate = new HashMap<String, String>();
        XMLOutputFactory xmlOutputFactory;
        try {
            xmlOutputFactory = (XMLOutputFactory) new XmlOutputFactoryFactoryBean(true).getObject();
        } catch (Exception e) {
            throw new RuntimeException("Error occurred trying to construct a XML output factory", e);
        }
        this.htmlParagraphStripper = new HTMLParagraphStripper(xmlOutputFactory, new DefaultXmlEventReaderFactory());
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        I18NBean i18NBean = this.getI18NBeanFactory().getI18NBean(getLocale());
        String spaceKey, pageTitle = "";
        outputType = context.getOutputType();
        Page includePage = null;

        // page include in space
        String pageParam = parameters.get(PAGE_PARAMETER).trim();
        if (context.getEntity() instanceof Draft) {
            spaceKey = ((Draft) context.getEntity()).getDraftSpaceKey();
        } else {
            spaceKey = ((Page) context.getEntity()).getSpaceKey();
        }
        includePage = this.pageManager.getPage(spaceKey, pageParam);

        // page include in another space
        if (includePage == null) {
            if (pageParam.indexOf(":") > 0) {
                int endSubIndex = pageParam.indexOf(":");
                spaceKey = pageParam.substring(0, endSubIndex);
                pageTitle = pageParam.substring(endSubIndex + 1);
            }
            includePage = this.pageManager.getPage(spaceKey, pageTitle);
        }

        if (includePage == null) {
            return i18NBean.getText("page.not.found.error");
        }

        String bodyResult = "";
        try {
            PageContext pageContext = new PageContext(includePage, context.getPageContext());
            pageContext.setOutputType(context.getOutputType());
            DefaultConversionContext defaultConversionContext = new DefaultConversionContext(pageContext);
            defaultConversionContext.setContentTree(context.getContentTree());

            String strippedBody = this.htmlParagraphStripper.stripFirstParagraph(includePage.getBodyAsString());
            bodyResult = this.getViewRenderer().render(strippedBody, defaultConversionContext);
        } catch (XMLStreamException ex) {
            Logger.getLogger(KodIncludePage.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bodyResult;
    }

    @Override
    public BodyType getBodyType() {
        return Macro.BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return Macro.OutputType.BLOCK;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="get/set">
    /**
     * @return the pageManager
     */
    public PageManager getPageManager() {
        return pageManager;
    }

    /**
     * @param pageManager the pageManager to set
     */
    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    /**
     * @return the i18NBeanFactory
     */
    public I18NBeanFactory getI18NBeanFactory() {
        return i18NBeanFactory;
    }

    /**
     * @param i18NBeanFactory the i18NBeanFactory to set
     */
    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory) {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    /**
     * @return the localeManager
     */
    public LocaleManager getLocaleManager() {
        return localeManager;
    }

    /**
     * @param localeManager the localeManager to set
     */
    public void setLocaleManager(LocaleManager localeManager) {
        this.localeManager = localeManager;
    }

    public Locale getLocale() {
        return localeManager.getLocale(getAuthenticatedUser());
    }

    public ConfluenceUser getAuthenticatedUser() {
        return AuthenticatedUserThreadLocal.get();
    }

    /**
     * @return the viewRenderer
     */
    public Renderer getViewRenderer() {
        return viewRenderer;
    }

    /**
     * @param viewRenderer the viewRenderer to set
     */
    public void setViewRenderer(Renderer viewRenderer) {
        this.viewRenderer = viewRenderer;
    }

    /**
     * @return the formatConverter
     */
    public FormatConverter getFormatConverter() {
        return formatConverter;
    }

    /**
     * @param formatConverter the formatConverter to set
     */
    public void setFormatConverter(FormatConverter formatConverter) {
        this.formatConverter = formatConverter;
    }
    // </editor-fold>
}
