var count = 0;

// 既存の共通部品、定形表を消すためインターバル処理を設定
var currentPointerPosition;

AJS.$(document).ready(function () {
    var actionForm = $('#createpageform');
    if (!actionForm) {
        actionForm = $('#editpageform');
    }
    if (actionForm) {
        setTimeout(function () {
            initIncludeMenu();
        }, 500);
    }

    function initIncludeMenu() {
        InitToolbar();
    }
});

/**
 * ツールバーの作成
 * @returns なし
 */
function InitToolbar() {
    //ツールバーのHTMLの設定
    var includecommon_menu_parts = Confluence.Templates.IncludeCommon.IncludeCommonMenuButton();

    //ツールバーの作成
    $("ul.rte-toolbar-group-justification").after(includecommon_menu_parts);

    //イベント設定
    $('body').on('mousedown', "#kod-includecommon", function (e) {
        currentPointerPosition = tinyMCE.activeEditor.selection.getBookmark(2, true);
        IncludeCommonOpenWindow();
        e.stopPropagation();
    });
}

/**
 * ポップアップの開通
 * @returns なし
 */
function IncludeCommonOpenWindow() {
    // 画面をブラックアウト
    $("body").append("</style><div class=\"aui-blanket\" tabindex=\"0\" aria-hidden=\"false\"></div><div class=\"kodloading\"></div>");

    // 画面作成
    var strMacroName = AJS.I18n.getText("includecommon.description.common");
    var headHTML = Confluence.Templates.IncludeCommon.IncludeCommonHeader();
    var bodyHTML = Confluence.Templates.IncludeCommon.IncludePageMacroTemplete({macroName: strMacroName});
    var footHTML = Confluence.Templates.IncludeCommon.IncludeCommonFooter();
    $('body').append(headHTML + bodyHTML + footHTML);
    $("#kod-include-input").css({
        "width": "1000px",
        "height": "600px",
        "z-index": "3012",
        "margin-top": "-300px",
        "margin-left": "-500px"
    });
    $(".kodloading").remove();

    //エベントの設定
    IncludeCommonWindowEvent();
    
    //disable Search when no input
    DisableSearchButton();
}

/**
 * エベントの設定
 * @returns なし
 */
function IncludeCommonWindowEvent() {
    //挿入ボタン押下時
    $("#kod-btn-insert").click(function (e) {
        var viewMenu = $(this).parents("#kod-include-input");
        var srcpageId = viewMenu.find("#kod-search-result option:selected").val();

        if (typeof srcpageId === "undefined" || srcpageId === "") {
            return false;
        }
        var dstpageId = AJS.params.pageId;

        var request = {
            srcPageId: srcpageId,
            destPageId: dstpageId
        };

        var baseUrl = AJS.General.getBaseUrl();
        var urltext = baseUrl + "/plugins/servlet/kod/includecommon/commonparts";
        $.ajax({
            url: urltext,
            type: "post",
            dataType: "json",
            data: request,
            success: function (res) {
                if (tinymce.isIE) {
                    tinyMCE.activeEditor.selection.moveToBookmark(currentPointerPosition);
                    tinyMCE.execCommand('mceInsertContent', false, res.html);
                } else {
                    tinyMCE.activeEditor.execCommand("mceInsertContent", false, res.html);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            },
            complete: function () {
                IncludeCommonCloseWindow();
                e.stopPropagation();
            }
        });
        e.stopPropagation();
    });

    //キャンセルボタン押下時
    $("#kod-btn-cancel").click(function (e) {
        IncludeCommonCloseWindow();
        e.stopPropagation();
    });
    
    IncludeSearchEvent();
}

/**
 * ポップアップの終了
 * @returns なし
 */
function IncludeCommonCloseWindow() {
    $(".aui-blanket").remove();
    $("#kod-include-input").remove();
    $("#kod-btn-insert").unbind();
    $("#kod-btn-cancel").unbind();
    $("#wysiwygTextarea_ifr").contents().find("body").focus();
}