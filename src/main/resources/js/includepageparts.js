/* global tinymce, AJS, Confluence */

//bind on initialization of editor
AJS.bind("init.rte", function () {
    var macroName = 'kodIncludePage';

    // 1. create dialog to add macro
    var dialog = new AJS.Dialog({
        width:1000,
        height:600,
        id:"kod-include-page"
    });
    
    // adds header for first page
    dialog.addHeader(AJS.I18n.getText("kod.plugin.includepage.macro.tilte"));

    // render macro body
    var strMacroName = AJS.I18n.getText("kod.plugin.includepage.macro.desc");
    var macroBody = Confluence.Templates.IncludeCommon.IncludePageMacroTemplete({macroName: strMacroName});
    dialog.addPanel("SinglePanel", macroBody, "includePagePanel");
    
    var searchContaner = $("#kod-search-result");
    searchContaner.empty();
    IncludeSearchEvent();

    // 2. add macro to editor
    var create = AJS.I18n.getText("kod.plugin.includepage.button.insert");
    dialog.addSubmit(create, function () {
        var selectPage = $(".selected-page").val();
        if (selectPage === '') {
            return false;
        }
        
        var currentParams = {};
        var viewMenu = $(this).parents(".aui-dialog");
        var page = viewMenu.find(".selected-page").val();
        currentParams["page"] = page;
        
        var macro = {
            name: macroName,
            params: currentParams,
            defaultParameterValue: "",
            body: ""
        };

        // 4. convert macro and insert in DOM        
        tinymce.confluence.macrobrowser.macroBrowserComplete(macro);
        $(".selected-page").val('');
        dialog.hide();
        /*tinymce.plugins.Autoconvert.convertMacroToDom(macro, function (data, textStatus, jqXHR) {
            AJS.$(selection).html(data);
            selection.html(data);
           
        }, function (jqXHR, textStatus, errorThrown) {
            AJS.log("error converting macro to DOM");
        });*/        
    });  
    
    // hide dialog
    var cancel = AJS.I18n.getText("kod.plugin.includepage.button.cancel");
    dialog.addCancel(cancel, function () {
        $(".selected-page").val('');
        dialog.hide();
    });

    // 5. bind event to open macro browser
    AJS.MacroBrowser.setMacroJsOverride(macroName, {opener: function (macro) {
            // get macro param
            var selection = AJS.Rte.getEditor().selection.getNode();
            var page = "";
            if (selection && macro.params) {
                var parameters = AJS.$(selection).attr("data-macro-parameters");
                if (parameters) {
                    var parametersarr = parameters.split("|");
                    page = parametersarr[0].split("=").pop();
                }
            }

            // open custom dialog
            dialog.show();
            $('.includePagePanel').css('padding', '0px');
            $('.kod-title').val('');
            $("#kod-search-result").empty();
            
            $("#kod-preview-result").contents().find("body").empty();
            $(".selected-page").val(page);
            if (selection && macro.params) {
                $(".kod-macro-browser-preview-link").trigger('click');
            }
        }
    });
});
