/* global AJS */
function DisableSearchButton() {
    $('.kod-btn-search').prop('disabled', true);
    $('input[type="text"]').on("input paste", function () {
        var viewMenu = $(this).parents(".include-macro");
        var spaceTitle = viewMenu.find("#kod-space-title").val().toString();
        var spaceCategory = viewMenu.find("#kod-space-category").val().toString();
        var pageTitle = viewMenu.find("#kod-page-title").val().toString();
        var pageLabel = viewMenu.find("#kod-page-label").val().toString();
        
        if ($.trim(spaceTitle) !== "" || $.trim(spaceCategory) !== "" || $.trim(pageTitle) !== "" || $.trim(pageLabel) !== "") {
            $('.kod-btn-search').prop('disabled', false);
        } else {
            $('.kod-btn-search').prop('disabled', true);
        }
    });
}

function IncludeSearchEvent() {
    //検索ボタン押下時
    $(".kod-btn-search").click(function (e) {
        var viewMenu = $(this).parents(".include-macro");
        var searchContaner = viewMenu.find("#kod-search-result");
        var dstpageId = AJS.params.pageId;
        executeProcessDisplay();
        var dataSearch = {
            spaceTitle: viewMenu.find("#kod-space-title").val(),
            spaceCategory: viewMenu.find("#kod-space-category").val(),
            pageTitle: viewMenu.find("#kod-page-title").val(),
            pageLabel: viewMenu.find("#kod-page-label").val(),
            destPageId: dstpageId
        };

        var baseUrl = AJS.General.getBaseUrl();
        var urltext = baseUrl + "/kod/includepage/search.action";
        $.ajax({
            url: urltext,
            type: "post",
            dataType: "json",
            data: dataSearch,
            async: false,
            success: function (res) {
                if (res.result === true) {
                    $(searchContaner).empty();
                    $(searchContaner).append(res.html);
                    $("#kod-preview-result").contents().find("body").empty();
                    
                    // Default render
                    var topItem = searchContaner.find("option:selected");
                    $('.selected-page').val(topItem.data('page'));
                    
                    // Preview when select page
                    $(searchContaner).find("select").on('change', function (e) {
                        var viewMenu = $(this).parents(".include-macro").parent();
                        var previewContaner = viewMenu.find("#kod-preview-result");
                        var pageParam = $(this).find('option:selected').data('page');
                        $('.selected-page').val(pageParam);
                        IncludePageSelectListResultEvent(previewContaner, pageParam);
                    });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("error occurred!\nXMLHttpRequest : " + XMLHttpRequest.status);
                $(".aui-blanket").remove();
            }
        });

        executeProcessClose();
        e.stopPropagation();
    });

    $(".kod-macro-browser-preview-link").click(function (e) {
        e.stopPropagation();
        var pageParam = $('.selected-page').val();
        var viewMenu = $(this).parents(".macro-preview-container");
        var previewContaner = viewMenu.find("#kod-preview-result");
        IncludePageSelectListResultEvent(previewContaner, pageParam);
    });
}

function IncludePageSelectListResultEvent(previewContaner, pageParam) {

    var dstpageId = AJS.params.pageId;
    var viewMenu = $(previewContaner).parents(".macro-preview-container");
    var previewLoading = viewMenu.find("#kod-include-preview-loading");
    var dstpageId = AJS.params.pageId;
    previewContaner.attr('src', '');

    if (pageParam !== "") {
        var dataSearch = {
            page: pageParam,
            destPageId: dstpageId
        };

        var baseUrl = AJS.General.getBaseUrl();
        var urltext = baseUrl + "/kod/includepage/preview.action";
        $.ajax({
            url: urltext,
            type: "post",
            dataType: "json",
            data: dataSearch,
            async: false,
            success: function (res) {
                if (res.result === true) {
                    var url = baseUrl + "/pages/viewpage.action?pageId=" + dstpageId;
                    previewLoading.html('<img style="width:100px;" src="../download/resources/jp.co.kodnet.confluence.plugins.include-common-parts-viewer:common-resources/images/loading_spinner.gif" />');
                    previewLoading.css("display", "flex");
                    previewContaner.css("display", "none");
                    previewContaner.attr('src', url);
                    previewContaner.load(function (e) {
                        e.stopPropagation();
                        previewContaner.contents().find("body").empty();
                        previewContaner.contents().find("body").append(res.html);
                        previewContaner.contents().find("body").attr("class", "content-preview");
                        previewContaner.css("display", "block");
                        previewLoading.html('');
                        previewLoading.css("display", "none");
                    });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("error occurred!\nXMLHttpRequest : " + XMLHttpRequest.status);
                $(".aui-blanket").remove();
            }
        });
    } else {
        previewContaner.contents().find("body").empty();
    }
}

function executeProcessDisplay() {
    var blanket = '<div class="kod-include-search-blanket aui-blanket" tabindex="0" aria-hidden="false" style="z-index: 4980;"></div>';
    $('body').append(blanket);
    $('body').find('#kod-include-search-loading').html('<img style="top: 50%;left: 40%;position: absolute;z-index: 5000;" src="../download/resources/jp.co.kodnet.confluence.plugins.include-common-parts-viewer:common-resources/images/loading.gif" />');
    $('body').find('#kod-include-search-loading').css("display", "block");
}

function executeProcessClose() {
    $('body').find('.kod-include-search-blanket').remove();
    $('body').find('#kod-include-search-loading').html('');
    $('body').find('#kod-include-search-loading').css("display", "none");
}
