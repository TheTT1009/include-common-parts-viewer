
import org.apache.wink.common.annotations.Asset;
import org.junit.Assert;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author TheTT
 */
public class UnitTest {

    @Test
    public void testSubtring() throws Exception {
        String testString = "height=250|name=2. CachDoiUngCongViec.docx";
        String resultString = "2. CachDoiUngCongViec.docx";
        String methodResult = this.getNameOfMacro(testString, "name=");
        Assert.assertEquals(resultString, methodResult);
    }

    private String getNameOfMacro(String macroAttrPara, String nameMark) throws Exception {
        String macroAttrName;
        int beginIndex = macroAttrPara.indexOf(nameMark);
        int endIndex = macroAttrPara.indexOf("|", beginIndex + nameMark.length());

        if (beginIndex < -1) {
            return "";
        }
        if (endIndex > -1) {
            macroAttrName = macroAttrPara.substring(beginIndex + nameMark.length(), endIndex);
        } else {
            macroAttrName = macroAttrPara.substring(beginIndex + nameMark.length());
        }
        return macroAttrName;
    }
}
